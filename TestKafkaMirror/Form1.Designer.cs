﻿namespace TestKafkaMirror
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.txtLog1 = new System.Windows.Forms.TextBox();
            this.txtLog2 = new System.Windows.Forms.TextBox();
            this.btnProduce = new System.Windows.Forms.Button();
            this.btnConsume = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSourceUrl = new System.Windows.Forms.TextBox();
            this.txtTargetUrl = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblTotalProduce = new System.Windows.Forms.Label();
            this.lblTotalConsume = new System.Windows.Forms.Label();
            this.txtSourceTopic = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtMessage = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtUserCount = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtProduceInterval = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCousumeTimeout = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtOffsetFrom = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtConsumeInterval = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtTargetTopic = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnSwitch = new System.Windows.Forms.Button();
            this.btnClean = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtLog1
            // 
            this.txtLog1.Location = new System.Drawing.Point(20, 211);
            this.txtLog1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLog1.Multiline = true;
            this.txtLog1.Name = "txtLog1";
            this.txtLog1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLog1.Size = new System.Drawing.Size(595, 365);
            this.txtLog1.TabIndex = 0;
            // 
            // txtLog2
            // 
            this.txtLog2.Location = new System.Drawing.Point(19, 211);
            this.txtLog2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLog2.Multiline = true;
            this.txtLog2.Name = "txtLog2";
            this.txtLog2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLog2.Size = new System.Drawing.Size(644, 365);
            this.txtLog2.TabIndex = 1;
            // 
            // btnProduce
            // 
            this.btnProduce.Location = new System.Drawing.Point(20, 156);
            this.btnProduce.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnProduce.Name = "btnProduce";
            this.btnProduce.Size = new System.Drawing.Size(128, 41);
            this.btnProduce.TabIndex = 2;
            this.btnProduce.Text = "Start Produce";
            this.btnProduce.UseVisualStyleBackColor = true;
            // 
            // btnConsume
            // 
            this.btnConsume.Location = new System.Drawing.Point(19, 156);
            this.btnConsume.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnConsume.Name = "btnConsume";
            this.btnConsume.Size = new System.Drawing.Size(128, 41);
            this.btnConsume.TabIndex = 3;
            this.btnConsume.Text = "Start Conume";
            this.btnConsume.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Bootstrap Servers";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Bootstrap Servers";
            // 
            // txtSourceUrl
            // 
            this.txtSourceUrl.Location = new System.Drawing.Point(166, 28);
            this.txtSourceUrl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSourceUrl.Name = "txtSourceUrl";
            this.txtSourceUrl.Size = new System.Drawing.Size(449, 25);
            this.txtSourceUrl.TabIndex = 6;
            this.txtSourceUrl.Text = "13.76.250.158:9094";
            // 
            // txtTargetUrl
            // 
            this.txtTargetUrl.Location = new System.Drawing.Point(171, 28);
            this.txtTargetUrl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTargetUrl.Name = "txtTargetUrl";
            this.txtTargetUrl.Size = new System.Drawing.Size(492, 25);
            this.txtTargetUrl.TabIndex = 7;
            this.txtTargetUrl.Text = "13.70.47.144:9094";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(537, 169);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 15);
            this.label3.TabIndex = 8;
            this.label3.Text = "Total:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(586, 166);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 15);
            this.label4.TabIndex = 9;
            this.label4.Text = "Total:";
            // 
            // lblTotalProduce
            // 
            this.lblTotalProduce.AutoSize = true;
            this.lblTotalProduce.Location = new System.Drawing.Point(597, 169);
            this.lblTotalProduce.Name = "lblTotalProduce";
            this.lblTotalProduce.Size = new System.Drawing.Size(15, 15);
            this.lblTotalProduce.TabIndex = 10;
            this.lblTotalProduce.Text = "0";
            // 
            // lblTotalConsume
            // 
            this.lblTotalConsume.AutoSize = true;
            this.lblTotalConsume.Location = new System.Drawing.Point(647, 166);
            this.lblTotalConsume.Name = "lblTotalConsume";
            this.lblTotalConsume.Size = new System.Drawing.Size(15, 15);
            this.lblTotalConsume.TabIndex = 11;
            this.lblTotalConsume.Text = "0";
            // 
            // txtSourceTopic
            // 
            this.txtSourceTopic.Location = new System.Drawing.Point(166, 71);
            this.txtSourceTopic.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSourceTopic.Name = "txtSourceTopic";
            this.txtSourceTopic.Size = new System.Drawing.Size(449, 25);
            this.txtSourceTopic.TabIndex = 13;
            this.txtSourceTopic.Text = "dev-test";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 73);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 15);
            this.label5.TabIndex = 12;
            this.label5.Text = "Topic";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtMessage);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.txtUserCount);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtProduceInterval);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtLog1);
            this.groupBox1.Controls.Add(this.txtSourceTopic);
            this.groupBox1.Controls.Add(this.btnProduce);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtSourceUrl);
            this.groupBox1.Controls.Add(this.lblTotalProduce);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(12, 25);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(639, 595);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Source Kafka";
            // 
            // txtMessage
            // 
            this.txtMessage.Location = new System.Drawing.Point(232, 163);
            this.txtMessage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.Size = new System.Drawing.Size(277, 25);
            this.txtMessage.TabIndex = 20;
            this.txtMessage.Text = "hello";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(163, 166);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(63, 15);
            this.label15.TabIndex = 19;
            this.label15.Text = "Message";
            // 
            // txtUserCount
            // 
            this.txtUserCount.Location = new System.Drawing.Point(436, 118);
            this.txtUserCount.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtUserCount.Name = "txtUserCount";
            this.txtUserCount.Size = new System.Drawing.Size(73, 25);
            this.txtUserCount.TabIndex = 18;
            this.txtUserCount.Text = "1";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(288, 121);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(135, 15);
            this.label14.TabIndex = 17;
            this.label14.Text = "Concurrent Users";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(245, 121);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 15);
            this.label8.TabIndex = 16;
            this.label8.Text = "ms";
            // 
            // txtProduceInterval
            // 
            this.txtProduceInterval.Location = new System.Drawing.Point(166, 118);
            this.txtProduceInterval.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtProduceInterval.Name = "txtProduceInterval";
            this.txtProduceInterval.Size = new System.Drawing.Size(73, 25);
            this.txtProduceInterval.TabIndex = 15;
            this.txtProduceInterval.Text = "1000";
            this.txtProduceInterval.TextChanged += new System.EventHandler(this.txtProduceInterval_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 121);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(135, 15);
            this.label7.TabIndex = 14;
            this.label7.Text = "Produce Interval";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.txtCousumeTimeout);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.txtOffsetFrom);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtConsumeInterval);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.txtTargetTopic);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtLog2);
            this.groupBox2.Controls.Add(this.btnConsume);
            this.groupBox2.Controls.Add(this.lblTotalConsume);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtTargetUrl);
            this.groupBox2.Location = new System.Drawing.Point(728, 25);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(679, 595);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Mirror Kafka";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(547, 123);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(23, 15);
            this.label12.TabIndex = 24;
            this.label12.Text = "ms";
            // 
            // txtCousumeTimeout
            // 
            this.txtCousumeTimeout.Location = new System.Drawing.Point(467, 118);
            this.txtCousumeTimeout.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCousumeTimeout.Name = "txtCousumeTimeout";
            this.txtCousumeTimeout.Size = new System.Drawing.Size(74, 25);
            this.txtCousumeTimeout.TabIndex = 23;
            this.txtCousumeTimeout.Text = "5000";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(334, 123);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(127, 15);
            this.label13.TabIndex = 22;
            this.label13.Text = "Consume Timeout";
            // 
            // txtOffsetFrom
            // 
            this.txtOffsetFrom.Location = new System.Drawing.Point(270, 166);
            this.txtOffsetFrom.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtOffsetFrom.Name = "txtOffsetFrom";
            this.txtOffsetFrom.Size = new System.Drawing.Size(94, 25);
            this.txtOffsetFrom.TabIndex = 21;
            this.txtOffsetFrom.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(169, 169);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(95, 15);
            this.label11.TabIndex = 20;
            this.label11.Text = "From Offset";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(251, 122);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 15);
            this.label9.TabIndex = 19;
            this.label9.Text = "ms";
            // 
            // txtConsumeInterval
            // 
            this.txtConsumeInterval.Location = new System.Drawing.Point(171, 118);
            this.txtConsumeInterval.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtConsumeInterval.Name = "txtConsumeInterval";
            this.txtConsumeInterval.Size = new System.Drawing.Size(74, 25);
            this.txtConsumeInterval.TabIndex = 18;
            this.txtConsumeInterval.Text = "0";
            this.txtConsumeInterval.TextChanged += new System.EventHandler(this.txtConsumeInterval_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 122);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(135, 15);
            this.label10.TabIndex = 17;
            this.label10.Text = "Consume Interval";
            // 
            // txtTargetTopic
            // 
            this.txtTargetTopic.Location = new System.Drawing.Point(171, 73);
            this.txtTargetTopic.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTargetTopic.Name = "txtTargetTopic";
            this.txtTargetTopic.Size = new System.Drawing.Size(492, 25);
            this.txtTargetTopic.TabIndex = 15;
            this.txtTargetTopic.Text = "data-center-AKSSE.dev-test";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 15);
            this.label6.TabIndex = 14;
            this.label6.Text = "Topic";
            // 
            // btnSwitch
            // 
            this.btnSwitch.Location = new System.Drawing.Point(657, 77);
            this.btnSwitch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSwitch.Name = "btnSwitch";
            this.btnSwitch.Size = new System.Drawing.Size(65, 41);
            this.btnSwitch.TabIndex = 14;
            this.btnSwitch.Text = "<>";
            this.btnSwitch.UseVisualStyleBackColor = true;
            this.btnSwitch.Click += new System.EventHandler(this.btnSwitch_Click);
            // 
            // btnClean
            // 
            this.btnClean.Location = new System.Drawing.Point(657, 396);
            this.btnClean.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnClean.Name = "btnClean";
            this.btnClean.Size = new System.Drawing.Size(65, 41);
            this.btnClean.TabIndex = 14;
            this.btnClean.Text = "Clean All";
            this.btnClean.UseVisualStyleBackColor = true;
            this.btnClean.Click += new System.EventHandler(this.btnClean_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1419, 634);
            this.Controls.Add(this.btnClean);
            this.Controls.Add(this.btnSwitch);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "Test Mirror Maker";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtLog1;
        private System.Windows.Forms.TextBox txtLog2;
        private System.Windows.Forms.Button btnProduce;
        private System.Windows.Forms.Button btnConsume;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSourceUrl;
        private System.Windows.Forms.TextBox txtTargetUrl;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblTotalProduce;
        private System.Windows.Forms.Label lblTotalConsume;
        private System.Windows.Forms.TextBox txtSourceTopic;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtTargetTopic;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnSwitch;
        private System.Windows.Forms.Button btnClean;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtProduceInterval;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtConsumeInterval;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtOffsetFrom;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtCousumeTimeout;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtUserCount;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtMessage;
        private System.Windows.Forms.Label label15;
    }
}

