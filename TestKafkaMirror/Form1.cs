﻿using Confluent.Kafka;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestKafkaMirror
{
    public partial class Form1 : Form
    {
        CancellationTokenSource tokenProduce = new CancellationTokenSource();
        ManualResetEvent resetProduce = new ManualResetEvent(true);

        CancellationTokenSource tokenConsume = new CancellationTokenSource();
        ManualResetEvent resetConsume = new ManualResetEvent(true);

        public Form1()
        {
            InitializeComponent();

            FormClosing += formClosing;

            btnProduce.Click += btnProduce_Start;
            btnConsume.Click += btnConsume_Start;

            foreach (var item in groupBox1.Controls)
            {
                if (item is TextBox)
                {
                    try
                    {
                        var box = ((TextBox)item);
                        string saved = Properties.Settings.Default[box.Name].ToString();
                        box.Text = string.IsNullOrEmpty(saved) ? box.Text : saved;
                        box.TextChanged += textbox_TextChanged;
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            foreach (var item in groupBox2.Controls)
            {
                if (item is TextBox)
                {
                    try
                    {
                        var box = ((TextBox)item);
                        string saved = Properties.Settings.Default[box.Name].ToString();
                        box.Text = string.IsNullOrEmpty(saved) ? box.Text : saved;
                        box.TextChanged += textbox_TextChanged;
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }

        private void formClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        private void textbox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Properties.Settings.Default[((TextBox)sender).Name] = ((TextBox)sender).Text;
            }
            catch (Exception ex)
            {

            }
        }

        private void btnProduce_Start(object sender, EventArgs e)
        {
            tokenProduce = new CancellationTokenSource();
            btnProduce.Text = "Stop";
            btnProduce.Click += btnProduce_Stop;

            int users = int.Parse(txtUserCount.Text);
            string msg = "";
            string text = txtMessage.Text;
            for (int t = 1; t <= users; t++)
            {
                msg = $"message from user {t}: {text}";
                Task.Factory.StartNew(new Action<object>(Produce), msg, tokenProduce.Token);
            }


        }

        private void btnProduce_Stop(object sender, EventArgs e)
        {
            tokenProduce.Cancel();
            btnProduce.Text = "Produce";
            btnProduce.Click += btnProduce_Start;
        }

        private void btnConsume_Start(object sender, EventArgs e)
        {
            tokenConsume = new CancellationTokenSource();
            btnConsume.Text = "Stop";
            btnConsume.Click += btnConsume_Stop;
            Task.Run((Action)Consume, tokenConsume.Token);
        }

        private void btnConsume_Stop(object sender, EventArgs e)
        {
            tokenConsume.Cancel();
            btnConsume.Text = "Consume";
            btnConsume.Click += btnConsume_Start;
        }

        public async void Produce(object message)
        {
            string kafkaUrl = txtSourceUrl.Text;
            string topic = txtSourceTopic.Text;

            var config = new ProducerConfig { BootstrapServers = kafkaUrl };


            try
            {
                using (var p = new ProducerBuilder<Null, string>(config).Build())
                {
                    while (!tokenProduce.IsCancellationRequested)
                    {
                        try
                        {
                            var dr = await p.ProduceAsync(topic, new Message<Null, string> { Value = message.ToString() });

                            LogProduce($"Delivered '{dr.Value}' to '{dr.TopicPartitionOffset}'");
                            BeginInvoke(new Action(() =>
                            {
                                if (txtOffsetFrom.Text == "0")
                                    txtOffsetFrom.Text = dr.Offset.Value.ToString();
                                lblTotalProduce.Text = (int.Parse(lblTotalProduce.Text) + 1).ToString();
                            }));

                            Thread.Sleep(int.Parse(txtProduceInterval.Text));
                        }
                        catch (ProduceException<Null, string> e)
                        {
                            LogProduce($"Delivery failed: {e.Error.Reason}");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                LogProduce($"Error: {e.Message}");
            }

        }

        void LogProduce(string message)
        {
            this.txtLog1.BeginInvoke(new Action(() =>
            {
                this.txtLog1.AppendText(message);
                this.txtLog1.AppendText(Environment.NewLine);
            }));
        }


        public async void Consume()
        {
            string kafkaUrl = txtTargetUrl.Text;
            string topic = txtTargetTopic.Text;

            var conf = new ConsumerConfig
            {
                GroupId = "test-consumer-group",
                BootstrapServers = kafkaUrl,
                AutoOffsetReset = AutoOffsetReset.Earliest,
                EnableAutoCommit = false
            };

            using (var c = new ConsumerBuilder<Ignore, string>(conf).Build())
            {
                try
                {
                    //c.Subscribe(topic);
                    c.Assign(new TopicPartitionOffset(topic, 0, long.Parse(txtOffsetFrom.Text)));

                    while (!tokenConsume.IsCancellationRequested)
                    {
                        try
                        {
                            int timeout = int.Parse(txtCousumeTimeout.Text);

                            var cr = c.Consume(timeout);


                            if (cr != null && cr.Message != null)
                            {
                                LogConsume($"Consumed message '{cr.Message.Value}' at: '{cr.TopicPartitionOffset}'.");
                                BeginInvoke(new Action(() =>
                                {
                                    lblTotalConsume.Text = (int.Parse(lblTotalConsume.Text) + 1).ToString();
                                }));
                            }
                            else
                            {
                                LogConsume($"Failed to get message in {timeout}ms.");
                            }
                            Thread.Sleep(int.Parse(txtConsumeInterval.Text));
                        }
                        catch (ConsumeException e)
                        {
                            LogConsume($"Error occured: {e.Error.Reason}");
                        }
                        catch (Exception ex)
                        {
                            LogConsume($"Error occured: {ex.Message}");
                        }
                    }
                    c.Close();
                }
                catch (OperationCanceledException)
                {
                    c.Close();
                }
            }

        }

        void LogConsume(string message)
        {
            this.txtLog2.BeginInvoke(new Action(() =>
            {
                this.txtLog2.AppendText(message);
                this.txtLog2.AppendText(Environment.NewLine);
            }));

        }

        private void btnSwitch_Click(object sender, EventArgs e)
        {
            string urlSource = txtSourceUrl.Text;
            string topicSource = txtSourceTopic.Text;

            txtSourceUrl.Text = txtTargetUrl.Text;
            txtSourceTopic.Text = txtTargetTopic.Text;
            txtTargetUrl.Text = urlSource;
            txtTargetTopic.Text = topicSource;
        }

        private void btnClean_Click(object sender, EventArgs e)
        {
            lblTotalProduce.Text = "0";
            lblTotalConsume.Text = "0";
            txtLog1.Text = "";
            txtLog2.Text = "";
            txtOffsetFrom.Text = "0";
        }

        private void txtProduceInterval_TextChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txtProduceInterval.Text))
                txtProduceInterval.Text = "0";
        }

        private void txtConsumeInterval_TextChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txtConsumeInterval.Text))
                txtConsumeInterval.Text = "0";
        }


    }
}
